from flask import Flask, render_template, request
from flask import Flask, jsonify
import insightface
import urllib
import urllib.request
import cv2
import json
import numpy as np
from annoy import AnnoyIndex
import random
import math
import time
import cv2
from matplotlib import pyplot as plt
import mxnet as mx
from mxnet import autograd, gluon
import numpy as np
from gluoncv.utils import download, viz
import gluoncv as gcv
import gluonfr as gfr
from mtcnn_detector import MtcnnDetector
from PIL import Image
from collections import Counter

# Helper Functions
def find_key(input_dict, value):
    return next((k for k, v in input_dict.items() if v == value), None)
def find_value(input_dict, key):
    return next((v for k, v in input_dict.items() if k == key), None)
# Pre-process Image
detector = MtcnnDetector(model_folder='model')

# Uploading Pre-trained Model
model = insightface.model_zoo.get_model('arcface_r100_v1')
model.prepare(ctx_id=-1)
# Reading Stored JSON
# Restoring popularity assignment
# with open("popuarityAssignment.json","r") as f:
#     popularity = json.load(f)

# # Custom de-serializing class for numpy arrays
# def JsonNdarray(s_and_end, scan_once, _w=None, _ws=None):
#   np.asarray(json.JSONArray(s_and_end, scan_once, _w, _ws))
# class CustomDecoder(json.JSONDecoder):
#   def __init__(self,  *args, **kwargs):
#     json.JSONDecoder.__init__(self, *args, **kwargs)
#     self.parse_array = JsonNdarray

# # Restoring testing variables
# with open("dtst.json","r") as f:
#     dtst = json.load(f, cls=CustomDecoder)

# dtst = {int(k):v for k,v in dtst.items()}
# peepsTst = [find_key(popularity,q) for q in dtst.keys()]

# # Restoring training variables
# with open("dtrn.json","r") as f:
#     dtrn = json.load(f, cls=CustomDecoder)
# dtrn = {int(k):v for k,v in dtrn.items()}

# item_num = 0
# embs = []
# peepsTrn = {}
# mapping = {}
# for i in dtrn.keys():
#     for j in dtrn[i].keys():
#         item_num += 1
#         embs.append(list(dtrn[i][j][0]))
#         mapping[item_num] = j
#         peepsTrn[item_num] = find_key(popularity, i)

with open("peepsTrn.json","r") as f:
    peepsTrn = json.load(f)
def align(f_path):
    img = mx.image.resize_short(f_path, 400).asnumpy()
    out =detector.detect_face(img[:,:,::-1])
    if out is None:
        return cv2.resize(img,(112,112))
    for points in out[1]:
        for k in range(5):
            points[k] /= (img.shape[1]/img.shape[1])
            points[k+5] /= (img.shape[0]/img.shape[0])
    chip = detector.extract_image_chips(img, out[1], desired_size=112, padding=0.1)
    return model.get_embedding(chip[0])
# Using Pre-loaded model
u = AnnoyIndex(f=512, metric='angular')
u.load('test.ann')  # super fast, will just mmap the file

app = Flask(__name__)

@app.route('/')
def upload_file():
   return render_template('upload.html')


@app.route('/', methods=['POST'])
def uploader_file():
    
    if request.method == 'POST':
      f = request.files['file']
      str_img = f.read()
      img = mx.img.imdecode(str_img)
      test_item = align(img)
      sim = u.get_nns_by_vector(test_item[0], 10, include_distances=True)
      imdbs = [peepsTrn[x] for x in sim[0]]
      p = Counter(imdbs)
      score = sim[1]
      results =  jsonify(p)
      return results
if __name__ == '__main__':
   app.run(host='127.0.0.1',port='5000', debug=True)
