#%%
from PIL import Image
import face_recognition

#%%
# Load the jpg file into a numpy array
for filename in os.listdir('face_images'):
    dirName = os.path.splitext(filename)[0]
    fullDirName = os.path.join('identified',dirName)
    # Create target Directory if don't exist
    if not os.path.exists(fullDirName):
        os.mkdir(fullDirName)
        print("Directory ", fullDirName,  " Created ")
    else:
        print("Directory ", fullDirName,  " already exists")
    
    image = face_recognition.load_image_file("face_images/{}".format(os.path.basename(filename)))

    # Find all the faces in the image using a pre-trained convolutional neural network.
    # This method is more accurate than the default HOG model, but it's slower
    # unless you have an nvidia GPU and dlib compiled with CUDA extensions. But if you do,
    # this will use GPU acceleration and perform well.
    # See also: find_faces_in_picture.py

    face_locations = face_recognition.face_locations(image)

    print("I found {} face(s) in this photograph.".format(len(face_locations)))

    for face_num, face_location in enumerate(face_locations):

        # Print the location of each face in this image
        top, right, bottom, left = face_location
        print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(
            top, left, bottom, right))

        # You can access the actual face itself like this:
        face_image = image[top:bottom, left:right]
        pil_image = Image.fromarray(face_image)
        # display(pil_image)
        print(dirName)
        pil_image.save("{}/faces_{}.jpg".format(fullDirName,face_num))

#%%
